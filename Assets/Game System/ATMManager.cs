using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ATMManager : MonoBehaviour
{
    [SerializeField] GameObject depositButton;
    [SerializeField] GameObject withdrawButton;
    [SerializeField] TMP_InputField depositInputField;
    [SerializeField] TMP_InputField withdrawInputField;
    
    public void ToggleATMButtons()
    {
        if (!depositButton.activeSelf &&  !withdrawButton.activeSelf) 
        {
            depositButton.SetActive(true);
            withdrawButton.SetActive(true);
        }
        else
        {
            depositButton.SetActive(false);
            withdrawButton.SetActive(false);
        }
    }

    public void Deposit()
    {
        int depositAmount = Convert.ToInt32(depositInputField.text);
        CoinManager.coins -= depositAmount;
        CoinManager.bankAccBalance += depositAmount;
    }

    public void Withdraw()
    {
        int withdrawAmount = Convert.ToInt32(withdrawInputField.text);
        CoinManager.coins += withdrawAmount;
        CoinManager.bankAccBalance -= withdrawAmount;
    }
}
