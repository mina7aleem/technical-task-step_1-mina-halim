using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SetItemParent : MonoBehaviour
{
    [SerializeField] Transform playerItems;
    [SerializeField] Transform shopKeeper_1Items;
    [SerializeField] Transform shopKeeper_2Items;

    public void OnMouseDown()
    {
        if (transform.parent == shopKeeper_2Items || shopKeeper_1Items)
        {
            transform.Find("Sell To ShopKeeper_1 Button").gameObject.SetActive(false);
            transform.Find("Sell To ShopKeeper_2 Button").gameObject.SetActive(false);
            transform.Find("Buy Button").gameObject.SetActive(true);
        }
        if (transform.parent == playerItems)
        {
            transform.Find("Buy Button").gameObject.SetActive(false);
            transform.Find("Sell To ShopKeeper_1 Button").gameObject.SetActive(true);
            transform.Find("Sell To ShopKeeper_2 Button").gameObject.SetActive(true);
        }
    }

    public void OnBuyClick()
    {
        if (CoinManager.coins >= 100)
        {
            transform.SetParent(playerItems);
            transform.Find("Buy Button").gameObject.SetActive(false);
            CoinManager.coins -= 100;
        }
        else
        {
            Debug.Log("Player Have No Enough Money");
        }
        
    }

    public void OnSellToShopKeeper_1Click() 
    {
        transform.SetParent(shopKeeper_1Items);
        transform.Find("Sell To ShopKeeper_1 Button").gameObject.SetActive(false);
        transform.Find("Sell To ShopKeeper_2 Button").gameObject.SetActive(false);
        CoinManager.coins += 100;
    }

    public void OnSellToShopKeeper_2Click()
    {
        transform.SetParent(shopKeeper_2Items);
        transform.Find("Sell To ShopKeeper_1 Button").gameObject.SetActive(false);
        transform.Find("Sell To ShopKeeper_2 Button").gameObject.SetActive(false);
        CoinManager.coins += 100;
    }
}
