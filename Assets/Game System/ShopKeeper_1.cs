using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopKeeper_1 : MonoBehaviour
{
    [SerializeField] GameObject shopKeeper_1Inventory;
    [SerializeField] GameObject shopKeeper_2Inventory;

    public void OnMouseDown()
    {
        shopKeeper_2Inventory.SetActive(false);
        if (shopKeeper_1Inventory.activeSelf) 
        {
            shopKeeper_1Inventory.SetActive(false);
        }
        else
        {
            shopKeeper_1Inventory.SetActive(true);
        }
    }
}
