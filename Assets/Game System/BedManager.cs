using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BedManager : MonoBehaviour
{
    [SerializeField] GameObject awakeText;
    [SerializeField] GameObject sleepingText;
    [SerializeField] Image bed;

    public void OnBedClick()
    {
        sleepingText.SetActive(true);
        awakeText.SetActive(false);
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        while (bed.fillAmount > 0)
        {
            yield return new WaitForSeconds(0.1f);
            bed.fillAmount -= 0.01f;
        }
        if (bed.fillAmount < float.Epsilon)
        {
            bed.fillAmount = 1;
            CoinManager.bankAccBalance += 0.1 * CoinManager.bankAccBalance;
            sleepingText.SetActive(false);
            awakeText.SetActive(true);
        }
    }
}
