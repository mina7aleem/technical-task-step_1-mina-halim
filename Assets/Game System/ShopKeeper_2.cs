using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopKeeper_2 : MonoBehaviour
{
    [SerializeField] GameObject shopKeeper_1Inventory;
    [SerializeField] GameObject shopKeeper_2Inventory;
    
    public void OnMouseDown()
    {
        shopKeeper_1Inventory.SetActive(false);
        if (shopKeeper_2Inventory.activeSelf)
        {
            shopKeeper_2Inventory.SetActive(false);
        }
        else
        {
            shopKeeper_2Inventory.SetActive(true);
        }
    }
}
