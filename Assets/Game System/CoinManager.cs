using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    public static int coins = 1000;
    public static double bankAccBalance = 3000;
    [SerializeField] TextMeshProUGUI coinsText;
    [SerializeField] TextMeshProUGUI bankAccBalanceText;

    void Update()
    {
        DisplayCoins();
        DisplayBankAccBalance();
    }

    void DisplayCoins()
    {
        coinsText.text = $"Coins : {coins.ToString()} $";
    }

    void DisplayBankAccBalance()
    {
        bankAccBalanceText.text = $"Bank Acc. Balance : {bankAccBalance.ToString()} $";
    }
}
